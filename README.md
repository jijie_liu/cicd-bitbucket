# CI/CD for Angular project on Bitbucket

![logo](./images/logo.png)

This demo, an Angular project, explains how to set up a CI/CD pipeline on Bitbucket.

## Continuous Integration (CI)

When a Pull-request (PR) is made, Bitbucket will lanuch CI. CI includes:

- lint codes

- run all the unit tests

## Continuous Delivery (CD)

When the codes are merged into `main` branch, Bitbucket will lanuch CD. CD includes:

1. update the version of the application
2. build the newest application
3. replace the old application with the new one on the server

## Demo

check the [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

## References

- [How to do continuous deployment using bitbucket and Linux server](https://www.educative.io/edpresso/how-to-do-continuous-deployment-using-bitbucket-and-linux-server)

- [bitbucket pipeline setup](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/)
